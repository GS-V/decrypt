package romania;

public class CipherException extends Exception {
	private static final long serialVersionUID = 1984635833304780631L;

	public CipherException() {
    }
 
    public CipherException(String message, Throwable throwable) {
        super(message, throwable);
    }
}