package romania;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Main {

	private static final String CIPHER = "AES";
	
	public static void main(String... args) throws CipherException, IOException
	{
		String fileName = "/mnt/secret/flag_G.txt";
		
		encryptFile(getSeed(), new File(fileName));
	}

	private static void encryptFile(String secret, File plainTextFile) throws CipherException
	{
		File cipherTextFile = new File(plainTextFile.getAbsolutePath() + "_bak");
        try
        {
        	byte[] secretBytes = deriveKey(secret);
            Key key = new SecretKeySpec(secretBytes, CIPHER);
            Cipher cipher = Cipher.getInstance(CIPHER);
            cipher.init(1, key);
             
            FileInputStream inputStream = new FileInputStream(plainTextFile);
            byte[] inputBytes = new byte[(int) plainTextFile.length()];
            inputStream.read(inputBytes);
             
            byte[] outputBytes = cipher.doFinal(inputBytes);
             
            FileOutputStream outputStream = new FileOutputStream(cipherTextFile);
            outputStream.write(outputBytes);
             
            inputStream.close();
            outputStream.close();
            plainTextFile.delete();
             
        } 
        catch (Exception e)
        {
            throw new CipherException("Problem encrypting the file", e);
        }
    }
	
	private static byte[] deriveKey(String secret)
	{
		while(secret.length() < 32)
		{
			secret = secret + secret;
		}
		return Arrays.copyOfRange(secret.getBytes(), 0, 16);
	}

	private static String getSeed() throws IOException
	{
		try(BufferedReader reader = new BufferedReader(new FileReader(new File("./resources/seed.txt"))))
		{
			return reader.readLine();
		}
	}
}
